# ServiceHandleServer

## Description
Easy web server gui to handle a single systemd service. Using the 
remi.gui Python module. 

https://github.com/dddomodossola/remi

Its a standalone webserver gui so no other webserver is needed.

## Visuals


## Installation

## Usage

## Support
No

## Roadmap
No
## Contributing
Probably

## Authors and acknowledgment
This application would not pe possible with out the remi Python module.

https://github.com/dddomodossola/remi

## License
GNU General Public License v3.0

## Project status
Initial
