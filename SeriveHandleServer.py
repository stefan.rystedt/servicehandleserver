# Copyright (C) 2022 ryz
# 
# This file is part of servicehandleserver.
# 
# servicehandleserver is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# servicehandleserver is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with servicehandleserver.  If not, see <http://www.gnu.org/licenses/>.

from operator import sub
from sys import stderr
from typing import Tuple
import remi.gui as gui
from remi import start, App
import argparse
import subprocess
import re

height = "32px"

def handle_args():
    parser = argparse.ArgumentParser(description="Start a webgui server for handling single systemd unit")
    parser.add_argument('--address', '-a', default="0.0.0.0", type=str, help='The ip address to bind to')
    parser.add_argument('--port', '-p', default=4242, type=int, help='The port to use')
    parser.add_argument('--logfile', '-l', default=None, help='Log file to show')
    parser.add_argument('--username', '-u', default=None, help='User')
    parser.add_argument('--password', default=None, help='Password')
    parser.add_argument('service', type=str, help='The name of the systemd service to handle')
    return parser.parse_args()

class ServiceHandlerServer(App):
    def __init__(self, request, client_address, server, **app_args):
        super().__init__(request, client_address, server, **app_args)

    def main(self, service_name, log_file):
        self.service_name = service_name
        self.log_file = log_file
        print("Service: {}".format(service_name))
        container = gui.VBox()

        hbox = gui.HBox()

        self.service_lbl = gui.Label(text="{}: ".format(service_name), style={'font-size':height, 'background-color':'#a6a6a6', 'margin-right':'10px' })
        hbox.append(self.service_lbl)

        self.status_lbl = gui.Label(text="Stopped", style={'font-size':height})
        hbox.append(self.status_lbl)

        container.append(hbox)
        
        self.start_button = gui.Button(text="Start", style={'font-size':height, 'border-style':'groove', 'margin':'10px'})
        self.start_button.onclick.do(self.start_service)
        container.append(self.start_button)

        self.stop_button = gui.Button(text="Stop", style={'font-size':height, 'border-style':'groove', 'margin':'10px'})
        self.stop_button.onclick.do(self.stop_service)
        container.append(self.stop_button)

        self.status_button = gui.Button(text="Status", style={'font-size':height, 'border-style':'groove', 'margin':'10px'})
        self.status_button.onclick.do(self.update_status)
        container.append(self.status_button)

        self.detailed_lbl = gui.Label( style={'white-space':'pre', 'border-style':'solid', 'min-height': '320px', 'padding': '10px'})
        container.append(self.detailed_lbl)

        if self.log_file:
            self.logfile_lbl = gui.Label( style={'white-space':'pre', 'border-style':'solid', 'min-height': '320px', 'padding': '10px'})
            container.append(self.logfile_lbl)

        self.update_status()

        return container

    def update_status(self, emmiter=None):
        result = subprocess.run(['/usr/bin/sudo', 'systemctl', 'status', self.service_name], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        description = result.stdout.decode('utf-8')
        match = re.search(r'^\s*Active:([^\n]*)$', description, re.MULTILINE)
        if match:
            status = match.group(1).strip()
            self.status_lbl.set_text(status)
        else:
            self.status_lbl.set_text('Error')
        self.detailed_lbl.set_text(description)
        if self.log_file:
            try:
                with open(self.log_file) as log:
                    text = "".join(log.readlines()[-40:])
                    self.logfile_lbl.set_text(text)
            except: FileNotFoundError

    def start_service(self, emiter=None):
        subprocess.run(['/usr/bin/sudo', 'systemctl', 'start', self.service_name], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        self.update_status()

    def stop_service(self, emiter=None):
        subprocess.run(['/usr/bin/sudo', 'systemctl', 'stop', self.service_name], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        self.update_status()
        

if __name__ == "__main__":
    args = handle_args()
    start(ServiceHandlerServer, start_browser=False , username=args.username, password=args.password, address=args.address, port=args.port, userdata=(args.service, args.logfile))